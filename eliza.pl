#!/usr/bin/env perl

#Justin Miller
#CMSC 416
#Spring 2018
#Eliza is a model of a Rogerian Psychotherapist, which basically means
#it takes the 'patients' statements about themselves and turns them into 
#questions.
#
#This program can be run on *Nix systems by running ./eliza.pl
#Example
#./eliza.pl
#[eliza] : What is your name?
#[User] : My friends call me Bob.
#[eliza] : Hello Bob, what can I help you with today?
#[Bob] : I want to get better at talking to people.
#[eliza] : Could you put that another way?
#[Bob] : I wish I had more friends to talk to.
#[eliza] : What makes you wish for more friends to talk to?
#...
#I went about solving this problem by scanning the input for certain key
#words, and then either asking a question relating to that word or taking other
#information from the input (such as for more friends to talk to) to create and build
#a question.

$name="User";
$promptu="[$name] : ";
$promptm="[eliza] : ";
print "$promptm What is your name?\n$promptu";
$name= <STDIN>;
chomp $name;
if($name =~/^(\w+\s)*\bis\b\s(\w+)*/) { #name is blah
    $name = $+;
}
elsif ($name =~/^(\w+\s)*\bgo by\b\s(\w+)*/) #I go by blah
{
    $name = $+;
}
elsif ($name =~/^(\w+\s)*\bcalled\b\s(\w+)*/) #I am called blah
{
    $name = $+;
}
elsif ($name =~/^(\w+\s)*\bcall me\b\s(\w+)*/) #My friends call me blah
{
    $name = $+;
}
$promptu="[$name] : "; #Set prompt to input name
print "$promptm Hello $name, what can I help you with today?\n";
print "$promptu";
while (1 == 1)
{
    #I chose to look for the words want, crave, desire, need, wish, like, hate, feel, and hurt
    #Because I felt that most statements a therapist would be trying to get someone to make
    #could be written in terms of those words
    $in = <STDIN>;
    chomp $in;
    $in=~s/[[:punct:]]//; #I wanted to remove punctuation, so I don't have to worry about it in comparisons.
    if ($in =~/^(\w+\s)*\bwant\b\s(\w+)*/) 
    {
	#This block of code scans for the block of text after the filter word
	#It works by scanning until it finds the filter word, then popping the 
	#rest of the input onto the temp array.
	@arr = split(/\s+/,$in);
	@temp = ("");
	$i = 0;
	$b = "";
	while($i < @arr)
	{
	    if($arr[$i] =~m/want/)
	    {
		$b = "t";
		$i++;
		next;
	    }
	    if($b eq "t")
	    {
		push @temp, $arr[$i];
	    }
	    $i++;
	}
        $in = "Why do you want@temp?\n";
    }
    elsif ($in =~/^(\w+\s)*\bcrave\b\s(\w+)*/)
    {
        $in = "Would you please tell me more about your cravings?\n";
    }
    elsif ($in =~/^(\w+\s)*\bdesire\b\s(\w+)*/)
    {
        $in = "Can you tell me more about your desires?\n";
    }
    elsif ($in =~/^(\w+\s)*\bneed\b\s(\w+)*/) 
    {
	#This block of code scans for the block of text after the filter word
	@arr = split(/\s+/,$in);
	@temp = ("");
	$i = 0;
	$b = "";
	while($i < @arr)
	{
	    if($arr[$i] =~m/need/)
	    {
		$b = "t";
		$i++;
		next;
	    }
	    if($b eq "t")
	    {
		push @temp, $arr[$i];
	    }
	    $i++;
	}
 
        $in = "Tell me more about why you need@temp.\n";
    }
    elsif ($in =~/^(\w+\s)*\bwish\b\s(\w+)*/)
    {
	#This block of code scans for the block of text after the filter word
	@arr = split(/\s+/,$in);
	@temp = ("");
	$i = 0;
	$b = "";
	while($i < @arr)
	{
	    if($arr[$i] =~m/wish/)
	    {
		$b = "t";
		$i++;
		next;
	    }
	    if($b eq "t")
	    {
		push @temp, $arr[$i];
	    }
	    $i++;
	}

        $in = "What makes you wish for@temp?\n";
    }
    elsif ($in =~/^(\w+\s)*\blike\b\s(\w+)*/)
    {
	#This block of code scans for the block of text after the filter word
  	@arr = split(/\s+/,$in);
	@temp = ("");
	$i = 0;
	$b = "";
	while($i < @arr)
	{
	    if($arr[$i] =~m/like/)
	    {
		$b = "t";
		$i++;
		next;
	    }
	    if($b eq "t")
	    {
		push @temp, $arr[$i];
	    }
	    $i++;
	}

        $in = "Can you tell me more about why you like@temp?\n";
    }
    elsif ($in =~/^(\w+\s)*\bhate\b\s(\w+)*/)
    {
	#This block of code scans for the block of text after the filter word
	@arr = split(/\s+/,$in);
	@temp = ("");
	$i = 0;
	$b = "";
	while($i < @arr)
	{
	    if($arr[$i] =~m/hate/)
	    {
		$b = "t";
		$i++;
		next;
	    }
	    if($b eq "t")
	    {
		push @temp, $arr[$i];
	    }
	    $i++;
	}

        $in = "Why do you hate@temp?\n";
    }
    elsif ($in =~/^(\w+\s)*\bhurt\b\s(\w+)*/)
    {
        $in = "Could you explain why?\n";
    }
    elsif ($in =~/^(\w+\s)*\bfeel\b\s(\w+)*/)
    {
	#This block of code scans for the block of text after the filter word
	@arr = split(/\s+/,$in);
	@temp = ("");
	$i = 0;
	$b = "";
	while($i < @arr)
	{
	    if($arr[$i] =~m/feel/)
	    {
		$b = "t";
		$i++;
		next;
	    }
	    if($b eq "t")
	    {
		push @temp, $arr[$i];
	    }
	    $i++;
	}

        $in = "Why do you feel@temp?\n";
    }
    else {
	$in = "Could you put that another way?\n";
    }
    #Switch any instances of me or my to you/yours
    $in =~s/I/you/;
    $in =~s/me/you/;
    $in =~s/my/your/;
    #I reuse in as to build my output string, to save that little bit of memory.
    print "$promptm $in";
    print "$promptu";
}
