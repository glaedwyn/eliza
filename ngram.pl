#!/usr/bin/env perl

#Justin Miller
#CMSC 416
#Spring 2018
#
#This program takes in input numbers and files from the command line and builds a NGram model
#based on the input to emulate that style of writing/speech. It does so by using
#a statistical model based on the recent history of the word to develop a likelihood
#for it occuring.

$arglen = @ARGV;

$starttag = "";
$end="<end>";
$n = $ARGV[0];
$m = $ARGV[1];
print "Command line args : n = $n m = $m\n"; 
for $i(1...$n-1)
{
	$starttag .= "<start> ";
}
$fnum = 2;
open(SRC, "$ARGV[$fnum]") || die "Could not open $ARGV[$fnum].\n";
chomp($in);
#This while loop grabs all input files and sticks them together
while (1)
{
	$line = <SRC>;
	if(defined $line)
	{
		chomp($line);
		$in .= $line . " ";
	}
	elsif ($fnum < $#ARGV) # still not on the last file
	{
		$fnum++;
		open(SRC, "$ARGV[$fnum]") || die "Could not open $ARGV[$fnum].\n";
	}
	else 
	{
		last;
	}
}
#Unigrams!
if($n == 1)
{
	#insert Starttag at beginning of input.
	$in = $starttag ." " . $in;
	$in=~s/([\.|\!|\?])/ $1 $starttag/g;	#Pad sentence boundaries with starttag
	$in=~s/([^a-zA-Z0-9|<|>|\'|\s|\-])/ $1 /g; #pad punctuation with spaces to make them tokens
	$in = lc($in);
	@parse = split(/\s+/,$in);	#split in string on whitespace
	%freq = ();			#empty frequency counter
	foreach $word (@parse)		#Count each word
	{
		$freq{$word}++;	
	}
	$numel = $#parse;		#Get total number of words
	foreach $key (keys %freq)
	{
		$freq{$key} = %freq{$key}/$numel;	#make each word a percentage of total words
	}
	$built = 0;
	while ($built < $m) #begin building M sentences
	{
		$app = "";	#append variable
		$sent = "";	#sentence variable
		until ($app=~m/[\.|\!|\?]/)	#End of sentence characters
		{
			$num = rand();	#generate random number. Add probabilities until sum is greater
			$sum = 0;
			foreach $word (keys %freq)
			{
				if($word eq $starttag){next;}
				$app = $word;
				$sum += $freq{$word};
				if($sum > $num){last;}
			}
			if($app=~m/[\.|\!|\?]/ && $sent eq "")
			{
				$app = "";
				next;
			}
			$sent .= $app . " ";
		}
		print $sent . "\n";
		$built++;
	} # end building sentences
}
#NGrams
else
{
	#Using ~ instead of whitespace because of troubleshooting. Generating the term to look
	#at in the hashtable is not working with whitespace
	$in = $starttag . $in;
	$in=~s/([\.|\!|\?])/ $end $1 $starttag/g;
	$in=~s/([^a-zA-Z0-9|<|>|\'|\s|\-])/ $1 /g;
	$in = lc($in);
	@parse = split($starttag,$in);
	#split input based on the start variable, which is <start>~ n-1 times
	for $k (0 ... $#parse)
	{
		$parse[$k] = $starttag . $parse[$k];
	}
	#add back in start token for beginning of sentence generation at the end.
	%nfreq = ();	#N Gram freq
	%nlfreq = ();	#N-1 Gram freq
	foreach $sentence (@parse)
	{
		if($sentence eq "")
		{
			next;
		}
		@sent = split(/\s+/, $sentence);
		if($#sent < ($n*2)-1)	#sentence not long enough (padded with n-1 <start>)
		{
			next;
		}
		else
		{
			my $i = 0;
			my $j = $n-2; #Build N-1 Gram before N, using 0 indexing subtract 2 to get N-1 elements
			while($j < $#sent) #-2 because of 0 indexing and don't want to go outside array
			{
				#do n-1gram frequency then do ngram frequency
				my $nlkey = join(" ",@sent[$i...$j]);
				$nlfreq{$nlkey}++;
				my $nkey = $nlkey . " " . $sent[$j+1];
				$nfreq{$nkey}++;
				$i++;
				$j++;	
			}
		}
	}
	%Ngram;
	for $ngram (keys %nfreq)	#builds Ngram hash of hashes with probabilities
	{
		@arr = split(/\s+/, $ngram);
		$nkey = pop @arr;
		$nlkey = join(" ",@arr);
		$Ngram{$nlkey}{$nkey} = $nfreq{$ngram}/$nlfreq{$nlkey};
	}
	$built = 0;
	while($built < $m)
	{
		$sent = "";
		$app = "";
		$search = $starttag;
		chop $search;	#cut off trailing ~
		until($app eq $end) #go until the end tag is reached
		{
			$num = rand();
			$sum = 0;
			for $term (keys %{$Ngram{$search}})
			{
				$sum += $Ngram{$search}{$term};
				if ($sum > $num)
				{
					$app = $term;
					last;
				}
			}
			if($app eq '')
			{
				print "Found no string to append. Error. Exiting.";
				last;
			}
			$sent .= " " . $app;
			@arr = split(/\s+/,$search);
			shift @arr;
			push(@arr, $app);
			$search = join(" ", @arr);
		}
		$sent=~s/$end//;
		print "$sent\n";
		$built++;
	}

}
